package com.app.products.com.app.products.models.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.products.com.app.products.models.entity.Product;

public interface ProductRepository extends JpaRepository<Product, UUID>{
	
}
