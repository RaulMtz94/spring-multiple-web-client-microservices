package com.app.products.com.app.products.controllers;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.app.products.com.app.products.models.entity.Product;
import com.app.products.com.app.products.models.service.IProductServices;

@RestController
public class ProductController {

	@Autowired
	private IProductServices productService;

	@GetMapping("/list")
	public List<Product> list(){
		return productService.findAll();
	}
	
	@GetMapping("/detail/{id}")
	public Product detail(@PathVariable UUID id) {
		return productService.findById(id);
	}
	
}
