package com.app.products.com.app.products.models.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.products.com.app.products.models.entity.Product;
import com.app.products.com.app.products.models.repository.ProductRepository;

@Service
public class ProductServiceImpl implements IProductServices{

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	@Transactional(readOnly=true)
	public List<Product> findAll() {
		return productRepository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Product findById(UUID id) {
		return productRepository.findById(id).orElse(null);
	}

}
