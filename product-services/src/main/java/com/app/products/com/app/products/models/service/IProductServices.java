package com.app.products.com.app.products.models.service;

import java.util.List;
import java.util.UUID;

import com.app.products.com.app.products.models.entity.Product;

public interface IProductServices {
	
	public List<Product> findAll();
	public Product findById(UUID id);
	
}
