package com.app.item.controllers;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.app.item.models.Item;
import com.app.item.models.service.ItemService;

@RestController
public class itemController {
	
	@Autowired
	private ItemService itemService;
	
	@GetMapping("/list")
	public List<Item> list(){
		return itemService.findAll();
	}
	
	@GetMapping("/detail/{id}/quantity/{quantity}")
	public Item detail(@PathVariable UUID id, @PathVariable Integer quantity) {
		return itemService.findById(id, quantity);
	}
}
