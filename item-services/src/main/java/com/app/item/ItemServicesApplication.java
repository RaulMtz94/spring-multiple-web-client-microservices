package com.app.item;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
@EnableFeignClients
@SpringBootApplication
public class ItemServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItemServicesApplication.class, args);
	}

}
