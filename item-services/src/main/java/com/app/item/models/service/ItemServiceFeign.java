package com.app.item.models.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.app.item.clients.ProductRestClient;
import com.app.item.models.Item;
@Service
public class ItemServiceFeign implements ItemService {
	
	private ProductRestClient clientFeign;

	@Override
	public List<Item> findAll() {
		return clientFeign.list().stream().map(x -> new Item(x, 1)).collect(Collectors.toList());
	}

	@Override
	public Item findById(UUID id, Integer quantity) {
		return new Item(clientFeign.detail(id), quantity );
	}

}
