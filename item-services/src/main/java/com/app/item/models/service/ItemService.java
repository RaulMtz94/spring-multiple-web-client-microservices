package com.app.item.models.service;

import java.util.List;
import java.util.UUID;

import com.app.item.models.Item;

public interface ItemService {
	public List<Item> findAll();
	public Item findById(UUID id, Integer quantity);
}
