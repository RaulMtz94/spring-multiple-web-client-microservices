package com.app.item.models.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.app.item.models.Item;
import com.app.item.models.Product;

@Service
@Primary
public class itemServiceImpl implements ItemService {
	
	@Autowired
	private RestTemplate restClient;
	
	@Override
	public List<Item> findAll() {
		List<Product> products = Arrays.asList(restClient.getForObject("http://localhost:5000/list", Product[].class));
		return products.stream().map(x -> new Item(x, 1)).collect(Collectors.toList());
	}

	@Override
	public Item findById(UUID id, Integer quantity) {
		Map<String, String> pathVars = new HashMap<String, String>();
		pathVars.put("id", id.toString());
		Product product = restClient.getForObject("http://localhost:5000/detail/{id}", Product.class, pathVars);
		return new Item(product, quantity);
	}

}
